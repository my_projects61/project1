# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#%% IMPORTS 
import pandas as pd
import numpy as np
import sqlite3
import pandas as pd
import pandas_datareader as pdr
import random
import matplotlib.pyplot as plt
from scipy import stats
import statsmodels.api as sm
from sklearn import linear_model


#%% EX1
'''
The code to download data is taken from the previous  project. 
To perform the required tasks I used sqlite in python, creating a new test3.db and a connection 
between sqlite and python
'''


#%%%Dataset
'''
Create a Structural Query Language (SQL) database to store all fillings in 2021 from the U.S Securities
 and Exchange Commission Electronic Data Gathering, Analysis and Retrieval system (SEC EDGAR). 
 Discuss the database e.g. how many datapoints? how many firms? How many fillings per firm on average?
'''
import requests

#need to modify header as per lesson 4 
heads = {'Host': 'www.sec.gov', 'Connection': 'close','Accept': 'application/json, text/javascript, */*; q=0.01', 'X-Requested-With': 'XMLHttpRequest','User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36'}

ll=[] #need an empty list
for qtr in range(1,5): #create a loop to iterate over the quarters 
    url = f"https://www.sec.gov/Archives/edgar/full-index/2021/QTR{qtr}/master.idx" #this is another way to .format strings
    request_link = requests.get(url, headers= heads) 
    request_content = request_link.content
    result = request_content.decode("utf-8", "ignore")
    with open(f'master_index_2021QTR{qtr}.txt','w') as f:
        f.write(result) 
    with open(f'master_index_2021QTR{qtr}.txt','r') as f:
        lines = f.readlines()
    
    records = [list(line.split('|')) for line in lines[11:]] #from line 11 till the end 
    ll+=records #add the results correponding to the quarter in the specific loop.

    
df = pd.DataFrame(ll, columns=['CIK', 'Name','Form','Date','Location']) 


#%%% Solution 


#create database and a table for company name and ticker
connection = sqlite3.connect('test3.db')
cur = connection.cursor()

#cur.execute("CREATE TABLE Companies (CIK INTEGER, Company TEXT, Form TEXT, Date TEXT, Location TEXT);")

df.to_sql('Companies', connection, if_exists='replace', index= False) 

#------HOW MANY DATAPOINTS-------
cur.execute('''
            SELECT COUNT(*) FROM Companies
            ''')           
numberOfRows = cur.fetchone()[0]

#-------HOW MANY FIRMS----------

cur.execute('SELECT  COUNT (DISTINCT Name) FROM Companies')

numberOfCompanies = cur.fetchone()[0]

#-------AVERAGE FILINGS PER FIRM--------
counter=df.groupby('CIK').count()
counter['Name'].mean()
1

#connection.close()

#%% EX2
'''
Significant coporate events/announcments are:
    10-K
    10-Q
    8-K
    4
    see: https://www.investopedia.com/articles/fundamental-analysis/08/sec-forms.asp
'''
#%%% Solution
cur.execute('''
            SELECT * FROM Companies WHERE Form in ('10-K')
            ''')
form10_k = cur.fetchall()

cur.execute('''
            SELECT * FROM Companies WHERE Form in ('10-Q')
            ''')
form10_q = cur.fetchall()

cur.execute('''
            SELECT * FROM Companies WHERE Form in ('8-K')
            ''')
form8_k = cur.fetchall()

cur.execute('''
            SELECT * FROM Companies WHERE Form in ('4')
            ''')
form4 = cur.fetchall()



#%% EX3 

'''
Creating an unique dataset with thew 30 firms of choice, their prices and event/nonevent days
'''

#%%% Dataset Creation
ll= ['TRAVELERS COMPANIES, INC.','Walgreens Boots Alliance, Inc.','DOW INC.','3M CO',
     'AMERICAN EXPRESS CO','GOLDMAN SACHS GROUP INC','CATERPILLAR INC','IBM CREDIT LLC',
     'BOEING CO','AMGEN INC','HONEYWELL INTERNATIONAL INC', 'MCDONALDS CORP','CHEVRON CORP',
     'CISCO SYSTEMS, INC.','SALESFORCE.COM, INC.','COCA COLA CO', 'Merck & Co., Inc.',
     'NIKE, Inc.','INTEL CORP','VERIZON COMMUNICATIONS INC', 'HOME DEPOT, INC.', 'Walt Disney Co',
     'PROCTER & GAMBLE Co','United Health Products, Inc.','Walmart Inc.','JOHNSON & JOHNSON',
     'JPMORGAN CHASE & CO','VISA INC.','MICROSOFT CORP', 'Apple Inc.']

tickers = ['TRV', 'WBA', 'DOW','MMM','AXP','GS', 'CAT','IBM', 'BA','AMGN','HON','MCD','CVX',
           'CSCO','CRM', 'KO','MRK','NKE','INTC','VZ', 'HD', 'DIS', 'PG','UEEC', 'WMT',
           'JNJ','JPM','V','MSFT', 'AAPL']
prices = pdr.DataReader(tickers,'yahoo','2021-1-1','2021-12-31')['Adj Close']

forms= [form10_k,form10_q,form4,form8_k]

data10K=pd.DataFrame(form10_k, columns=['CIK','NAME','FORM','DATE','LOCATION'])
data10Q=pd.DataFrame(form10_q, columns=['CIK','NAME','FORM','DATE','LOCATION'])
data4=pd.DataFrame(form4, columns=['CIK','NAME','FORM','DATE','LOCATION'])
data8K=pd.DataFrame(form8_k, columns=['CIK','NAME','FORM','DATE','LOCATION'])
            
dfs=[data10K,data10Q,data4,data8K]


prices.columns = ll

#%%%% Dataset Manipulation
dataforms = pd.concat(dfs)
dataforms=dataforms.drop(['CIK', 'LOCATION'], axis=1)
dataforms=dataforms.set_index('DATE')
dataforms= dataforms[dataforms.NAME.isin(ll)]
dataforms=dataforms.pivot_table(values='FORM', index= dataforms.index, columns= 'NAME', aggfunc='first')
dataforms=dataforms.fillna(value=0)
prices.index= prices.index.date
prices= prices.reindex(sorted(prices.columns), axis=1)
prices=prices.pct_change(1)

#%%% Event Dates and Prices

      
ll2=dataforms.loc[(dataforms["Apple Inc."] != 0), :]       
        

interest= abs(prices[:])>0.05
interest=interest.astype(int)

interest = interest.value_counts()


#%% EX4

#%%% Boeing

dummies = pd.get_dummies(dataforms['BOEING CO'])
dummies=dummies.drop(0,1)
dummies.index = pd.to_datetime(dummies.index)
dummies.index= dummies.index.date
df_boeing = pd.DataFrame(prices['BOEING CO'])

df_boeing_concat= pd.concat([df_boeing,dummies], axis=1)
df_boeing_concat=df_boeing_concat.fillna(value=0)

#%%%% Econometric Test on Boeing 

X= df_boeing_concat[['10-K', '10-Q','4', '8-K']]
y = df_boeing_concat['BOEING CO']

regr = linear_model.LinearRegression()
regr.fit(X, y)
print(regr.coef_)


#%%% Apple
dummies = pd.get_dummies(dataforms['Apple Inc.'])
dummies=dummies.drop(0,1)
dummies.index = pd.to_datetime(dummies.index)
dummies.index= dummies.index.date
df_boeing = pd.DataFrame(prices['Apple Inc.'])

df_boeing_concat= pd.concat([df_boeing,dummies], axis=1)
df_boeing_concat=df_boeing_concat.fillna(value=0)

#%%%% Econometric Test for Apple
X= df_boeing_concat[['10-K', '10-Q','4', '8-K']]
y = df_boeing_concat['Apple Inc.']

regr = linear_model.LinearRegression()
regr.fit(X, y)
print(regr.coef_)

#%%% MCDonalds 

dummies = pd.get_dummies(dataforms['MCDONALDS CORP'])
dummies=dummies.drop(0,1)
dummies.index = pd.to_datetime(dummies.index)
dummies.index= dummies.index.date
df_boeing = pd.DataFrame(prices['MCDONALDS CORP'])

df_boeing_concat= pd.concat([df_boeing,dummies], axis=1)
df_boeing_concat=df_boeing_concat.fillna(value=0)

#%%%% McDonalds
X= df_boeing_concat[['10-K', '10-Q','4', '8-K']]
y = df_boeing_concat['MCDONALDS CORP']

regr = linear_model.LinearRegression()
regr.fit(X, y)
print(regr.coef_)



